# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "0.9.0"
  end
end
